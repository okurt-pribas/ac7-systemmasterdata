/**
 *   Oray KURT
 *   11-May-2018
 *   destinymasterdata
 */

Ext.define('AC7.modules.systemmasterdata.App', {
    extend: 'Ext.ux.desktop.Module',
    
    controllers: [
        'AC7.modules.systemmasterdata.app.controller.MainController'
    ],
    
    requires: [],
    
    id: 'systemmasterdata-win',
    
    // configuration: {
    //  ref : 'AC7Gocart2',
    //  url : 'gocart/getConfig'
    // },
    
    init: function () {
        Ext.util.CSS.swapStyleSheet('systemmasterdata',
            'app/modules/systemmasterdata/resources/css/systemmasterdata.css');
        
        this.launcher = {
            text: getText('L_MODULE_TITLE_SYSTEMMASTERDATAUI', 'System Master Data'),
            iconCls: 'icon-systemmasterdata'
        };
        
        this.shortcut = {
            name: getText('L_MODULE_TITLE_SYSTEMMASTERDATAUI', 'System Master Data'),
            iconCls: 'icon-systemmasterdata-shortcut',
            module: this.id
        };
        
    },
    
    createWindow: function (desktop) {
        var win = desktop.getWindow(this.id);
        if (!win) {
            win = desktop.createWindow({
                id: this.id,
                title: 'System Master Data',
                width: 1000,
                height: 600,
                name: 'md-MainWindow',
                alias: 'widget.md-MainWindow',
                x: 70,
                y: 30,
                iconCls: 'icon-systemmasterdata',
                layout: {
                    type: 'border',
                    align: 'stretch'
                },
                items: [
                    {
                        xtype: 'md-MainPanel',
                        autoScroll: true,
                        region: 'center'
                    }
                ],
                listeners: {
                    beforeclose : function () {
                        Ext.getStore('AC7.modules.systemmasterdata.app.store.GridAvailableStore').removeAll();
                        Ext.getStore('AC7.modules.systemmasterdata.app.store.GridActiveStore').removeAll();
                    }
                }
            });
        }
        return win;
    }
});