/**
 *   Oray KURT
 *   15-May-2018
 *   systemmasterdata.app.view
 */


Ext.define('AC7.modules.systemmasterdata.app.view.MasterDataMain', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.md-MainPanel',
    name: 'md-MainPanel',
    border: false,
    bodyCls: 'x-panel-body-default-framed',
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    initComponent: function () {
        this.items = [
            {
                xtype: 'md-Tree'
            },
            {
                xtype: 'md-Active'
            },
            {
                xtype: 'md-Available'
            }
        ];
        this.callParent(arguments);
    }
});