/**
 *   Oray KURT
 *   15-May-2018
 *   systemmasterdata.app.view
 */

Ext.define('AC7.modules.systemmasterdata.app.view.MasterDataTree', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.md-Tree',
    name: 'md-Tree',
    title: 'Objects',
    rootVisible: false,
    resizable: false,
    
    hideHeaders: true,
    sortable: false,
    frame: true,
    lastSelected: '',
    xtype: 'check-tree',
    flex: 1,
    dockedItems: {
        xtype: 'toolbar',
        dock: 'top',
        items: [
            {
                xtype: 'container',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [{
                    xtype: 'button',
                    text: 'Actions',
                    name: 'md-ActionsMenu',
                    iconCls: 'icon-systemmasterdata-actions',
                    menu: [
                        {
                            xtype: 'button',
                            text: 'Import',
                            hidden: true,
                            iconCls: 'icon-systemmasterdata-import',
                            listeners: {
                                click: function (button) {
                                    var fileField = Ext.ComponentQuery.query('filefield[name=md-FileUpload]')[0];
                                    fileField.setVisible(true);
                                    fileField.fileInputEl.dom.click();
                                    fileField.setVisible(false);
                                },
                                beforerender: function (button) {
                                    if (AC7.Globals.hasBooleanPermission("masterdata.import")) {
                                        button.setVisible(true);
                                    }
                                }
                            }
                        }, {
                            xtype: 'button',
                            text: 'Export',
                            iconCls: 'icon-systemmasterdata-export',
                            listeners: {
                                click: function (button) {
                                    var tree = Ext.ComponentQuery.query('[name=md-Tree]')[0];
                                    var lastSelected = Ext.ComponentQuery.query('[name=md-Tree]')[0].getSelectionModel().getSelection()[0];
                                    if (lastSelected) {
                                        var oType;
                                        if (lastSelected.parentNode.raw.id === "root") {
                                            oType = lastSelected.raw.type;
                                            window.open("systemmasterdata/getCsvFile/objectType=" + oType);
                                        } else if (lastSelected.raw.leaf) {
                                            var sID = lastSelected.raw.type;
                                            oType = lastSelected.parentNode.raw.type;
                                            //"objectType=" + getObjectItems.getParent().getType() + "&&SID=" + getObjectItems.getSelf().getType();
                                            var objectType = "objectType=" + oType + "&&SID=" + sID;
                                            window.open("systemmasterdata/getCsvFile/" + objectType);
                                        }
                                    } else {
                                        window.open("systemmasterdata/getCsvFile");
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'filefield',
                            name: 'md-FileUpload',
                            buttonOnly: true,
                            hideLabel: true,
                            hidden: true
                        }
                    ]
                }]
            }, {
                xtype: 'container',
                flex: 1
            }, {
                xtype: 'button',
                iconCls: 'icon-systemmasterdata-reload',
                name: 'md-ReloadTree'
            }
        ]
    }
});


