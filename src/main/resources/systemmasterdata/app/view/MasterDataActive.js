/**
 *   Oray KURT
 *   16-May-2018
 *   systemmasterdata.app.view
 */

Ext.define('AC7.modules.systemmasterdata.app.view.MasterDataActive', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.md-Active',
    name: 'md-Active',
    frame: true,
    flex: 2,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    
    initComponent: function (args) {
        
        var me = this;
        
        this.items = [
            {
                xtype: 'grid',
                title: 'Active',
                store: 'AC7.modules.systemmasterdata.app.store.GridActiveStore',
                name: 'md-ActiveGrid',
                cls: 'systemmasterdata-grid',
                isActive: true,
                requires: [
                    'Ext.ux.RowExpander'
                ],
                resizeable: false,
                disabled: true,
                layout: 'fit',
                selModel: {
                    mode: 'MULTI'
                },
                emptyText: 'There are not any Active Data',
                flex: 1,
                columns: [
                    {
                        text: 'Code',
                        dataIndex: 'code',
                        flex: 2
                    },
                    {
                        text: 'Lang',
                        flex: 1,
                        renderer: function (value, metaData, record) {
                            
                            var name = _.findWhere(record.get('names'), {lang: AC7.Globals.clientConfig.user.selectedLang});
                            if (name) {
                                return name.lang;
                            }
                            
                            var nameEn = _.findWhere(record.get('names'), {lang: 'en'});
                            if (nameEn) {
                                return nameEn.lang;
                            }
                            
                            if (!Ext.isEmpty(record.get('names'))) {
                                return record.get('names')[0].lang;
                            }
                            return '';
                        }
                    },
                    {
                        text: 'Description',
                        flex: 6,
                        renderer: function (value, metaData, record) {
                            
                            var nameObj = _.findWhere(record.get('names'), {lang: AC7.Globals.clientConfig.user.selectedLang});
                            if (nameObj) {
                                return nameObj.name;
                            }
                            
                            var nameEn = _.findWhere(record.get('names'), {lang: 'en'});
                            if (nameEn) {
                                return nameEn.name;
                            }
                            
                            if (!Ext.isEmpty(record.get('names'))) {
                                return record.get('names')[0].name;
                            }
                            return '';
                        }
                    },
                    {
                        text: 'Origin',
                        dataIndex: 'source',
                        flex: 2
                    },{
                        xtype: 'actioncolumn',
                        width: 25,
                        name: 'md-ActionColumnEdit',
                        hidden: true,
                        listeners: {
                            beforerender: function (actionColumn) {
                                if (AC7.Globals.hasBooleanPermission("masterdata.sys.edit")) {
                                    actionColumn.setVisible(true);
                                }
                            }
                        },
                        items: [
                            {
                                xtype: 'button',
                                iconCls: 'icon-systemmasterdata-edit'
                            }
                        ]
                    }
                ],
                plugins: [
                    {
                        ptype: 'rowexpander',
                        pluginId: 'md-GridRowExpander',
                        rowBodyTpl: new Ext.XTemplate(
                            '<dl class = "systemmasterdata-grid-rowexpander">',
                            '<tpl for="names">',
                            '<dt  >{lang}- {name}</dt>',
                            '</tpl>',
                            "<tpl if='remark != \"\"'>",
                            '<dt>Remark: {remark}</dt>',
                            "</tpl>",
                            '</dl>'),
                        expandRow: function(rowIdx) {
                            var rowNode = this.view.getNode(rowIdx),
                                row = Ext.get(rowNode),
                                nextBd = Ext.get(row).down(this.rowBodyTrSelector),
                                record = this.view.getRecord(rowNode),
                                grid = this.getCmp();
                            if (row.hasCls(this.rowCollapsedCls)) {
                                row.removeCls(this.rowCollapsedCls);
                                nextBd.removeCls(this.rowBodyHiddenCls);
                                this.recordsExpanded[record.internalId] = true;}
                        },
    
                        collapseRow: function(rowIdx) {
                            var rowNode = this.view.getNode(rowIdx),
                                row = Ext.get(rowNode),
                                nextBd = Ext.get(row).down(this.rowBodyTrSelector),
                                record = this.view.getRecord(rowNode),
                                grid = this.getCmp();
                            if (!row.hasCls(this.rowCollapsedCls)) {
                                row.addCls(this.rowCollapsedCls);
                                nextBd.addCls(this.rowBodyHiddenCls);
                                this.recordsExpanded[record.internalId] = false;
                            }
                        }
                    }
                ],
                
                viewConfig: {
                    plugins: [
                        {
                            ptype: 'gridviewdragdrop',
                            dragText: 'Drag and Drop to deactivate.',
                            dragGroup: Ext.ComponentQuery.query('[name=md-AvailableGrid]')[0],
                            dropGroup: Ext.ComponentQuery.query('[name=md-ActiveGrid]')[0]
                            
                        }
                    ],
                    listeners: {
                        drop: function (node, ownGrid, targetGrid) {
                            me.fireEvent('drop', ownGrid);
                        }
                    },
                    getRowClass: function (record, rowIndex, rowParams, store) {
                        if (record.get('active') === false) {
                            return 'md-customGridRowHighlight';
                        }
                    }
                },
                
                dockedItems: [
                    {
                        xtype: 'toolbar',
                        dock: 'top',
                        items: [{
                            xtype: 'container',
                            items: {
                                xtype: 'button',
                                iconCls: 'icon-systemmasterdata-add-image',
                                text: 'Add',
                                name: 'md-addData'
                            }
                        }, {
                            xtype: 'container',
                            flex: 1
                        }, {
                            xtype: 'container',
                            items: [{
                                xtype: 'button',
                                name: 'md-ActiveExpand',
                                iconCls: 'icon-systemmasterdata-expand'
                            }, {
                                xtype: 'button',
                                name: 'md-ActiveCollapse',
                                iconCls: 'icon-systemmasterdata-collapse'
                            }]
                        }]
                    }
                ]
            }
        ];
        
        me.enableBubble('drop');
        
        this.callParent(args);
    }
});