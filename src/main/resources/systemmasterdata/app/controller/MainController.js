/**
 *   Oray KURT
 *   11-May-2018
 *   destinymasterdata.app.controller
 */

// AC7.Globals.hasBooleanPermission("masterdata.sys.edit")

Ext.define('AC7.modules.systemmasterdata.app.controller.MainController', {
    extend: 'AC7.controller.ModuleBaseController',
    views: [
        'AC7.modules.systemmasterdata.app.view.MasterDataMain',
        'AC7.modules.systemmasterdata.app.view.MasterDataTree',
        'AC7.modules.systemmasterdata.app.view.MasterDataActive',
        'AC7.modules.systemmasterdata.app.view.MasterDataAvailable'
    ],
    stores: [
        'AC7.modules.systemmasterdata.app.store.GridStore',
        'AC7.modules.systemmasterdata.app.store.GridAvailableStore',
        'AC7.modules.systemmasterdata.app.store.GridActiveStore',
        'AC7.modules.systemmasterdata.app.store.GridWarningStore'
    ],
    models: [
        'AC7.modules.systemmasterdata.app.model.TreeModel',
        'AC7.modules.systemmasterdata.app.model.GridModel'
    ],
    
    init: function () {
        this.control({
            'md-Active button[name=md-addData]': {
                click: this.addButtonEvent
            },
            
            'md-Tree filefield[name=md-FileUpload]': {
                change: this.importCsvField
            },
            
            'md-Available > grid[name=md-AvailableGrid] actioncolumn[name=md-ActionColumnEdit]': {
                click: this.onGridActionColumnEditEvent
            },
            
            'md-Active > grid[name=md-ActiveGrid] actioncolumn[name=md-ActionColumnEdit]': {
                click: this.onGridActionColumnEditEvent
            },
            
            'md-Available > grid[name=md-AvailableGrid] actioncolumn[name=md-ActionColumnDelete]': {
                click: this.onGridActionColumnDeleteEvent
            },
            
            'md-Available': {
                drop: this.changePosition
            },
            
            'md-Active': {
                drop: this.changePosition
            },
            
            'md-Active > grid[name=md-ActiveGrid] button[name=md-ActiveCollapse]': {
                click: this.onGridCollapseAll
            },
            
            'md-Available > grid[name=md-AvailableGrid] button[name=md-AvailableCollapse]': {
                click: this.onGridCollapseAll
            },
            
            'md-Active > grid[name=md-ActiveGrid] button[name=md-ActiveExpand]': {
                click: this.onGridExpandAll
            },
            
            'md-Available > grid[name=md-AvailableGrid] button[name=md-AvailableExpand]': {
                click: this.onGridExpandAll
            },
            
            'md-Tree': {
                itemclick: this.loadGrids,
                afterrender: this.treeViewRefresh
            },
            
            'md-Tree button[name=md-ReloadTree]': {
                click: this.reloadTreeButton
            }
            
        });
    },
    
    
    reloadTreeButton: function () {
        this.treeViewRefresh(Ext.ComponentQuery.query('[name=md-Tree]')[0]);
    },
    
    
    treeViewRefresh: function (treePanel) {
        var mainWindow = treePanel.up('window[name=md-MainWindow]');
        var availableGrid = Ext.ComponentQuery.query('[name=md-AvailableGrid]')[0];
        var activeGrid = Ext.ComponentQuery.query('[name=md-ActiveGrid]')[0];
        availableGrid.getStore().removeAll();
        activeGrid.getStore().removeAll();
        availableGrid.setDisabled(true);
        activeGrid.setDisabled(true);
        treePanel.setLoading(true);
        Ext.Ajax.request({
            url: 'systemmasterdata/getObjectTreeItems',
            method: 'GET',
            success: function (response) {
                var resp = Ext.JSON.decode(response.responseText);
                if (resp.data) {
                    if (resp.data.responseCode === 'I_OPERATION_SUCCESSFUL') {
                        var x, y;
                        var objectArray = [], yObjectArray = [];
                        var xObjectType = {}, yObjectType = {};
                        for (x = 0; x < resp.data.result.length; x++) {
                            if (resp.data.result[x].children) {
                                for (y = 0; y < resp.data.result[x].children.length; y++) {
                                    yObjectType = {
                                        text: resp.data.result[x].children[y].name,
                                        type: resp.data.result[x].children[y].type,
                                        leaf: true
                                    };
                                    yObjectArray.push(yObjectType);
                                }
                                xObjectType = {
                                    text: resp.data.result[x].name,
                                    type: resp.data.result[x].type,
                                    expanded: true,
                                    children: yObjectArray
                                };
                                objectArray.push(xObjectType);
                                yObjectArray = [];
                            } else {
                                xObjectType = {
                                    text: resp.data.result[x].name,
                                    type: resp.data.result[x].type,
                                    leaf: true
                                };
                                objectArray.push(xObjectType);
                            }
                        }
                        var tempStore = Ext.create('Ext.data.TreeStore', {
                            model: 'AC7.modules.systemmasterdata.app.model.TreeModel',
                            autoload: true,
                            autoSync: true,
                            root: {
                                type: 'objectType',
                                text: 'Object Types',
                                children: objectArray
                            }
                        });
                        Ext.ComponentQuery.query('[name=md-Tree]')[0].reconfigure(tempStore);
                        mainWindow.down('statusbar').setStatus({
                            text: 'Ready!',
                            iconCls: 'x-status-valid',
                            animate: true
                        });
                    } else {
                        mainWindow.down('statusbar').setStatus({
                            text: 'Ops! ' + resp.data.responseMessage,
                            iconCls: 'x-status-error',
                            animate: true
                        });
                    }
                } else {
                    mainWindow.down('statusbar').setStatus({
                        text: getText("E_SERVER_ERROR", "Server Error! Please contact system administrator."),
                        iconCls: 'x-status-error',
                        animate: true
                    });
                    treePanel.setDisabled(true);
                }
                treePanel.setLoading(false);
            },
            failure: function () {
                treePanel.setLoading(false);
                treePanel.setDisabled(true);
                treePanel.up('window[name=md-MainWindow]').down('statusbar').setStatus({
                    text: 'Internal Exception Error',
                    iconCls: 'x-status-error',
                    animate: true
                });
            }
        });
    },
    
    onGridExpandAll: function (button) {
        var rowExpander = button.up('grid').getPlugin("md-GridRowExpander");
        var nodes = rowExpander.view.getNodes();
        for (var i = 0; i < nodes.length; i++) {
            rowExpander.expandRow(i);
        }
    },
    
    onGridCollapseAll: function (button) {
        var rowExpander = button.up('grid').getPlugin("md-GridRowExpander");
        var nodes = rowExpander.view.getNodes();
        for (var i = 0; i < nodes.length; i++) {
            rowExpander.collapseRow(i);
        }
    },
    
    onGridActionColumnDeleteEvent: function (grid, cell, rowIndex, col, e) {
        
        Ext.Msg.show({
            title: 'QUESTION',
            msg: 'Do you really want to delete this?',
            icon: Ext.Msg.QUESTION,
            buttons: Ext.Msg.YESNO,
            fn: function (response) {
                if ('yes' === response) {
                    grid.up('window[name=md-MainWindow]').setLoading(true);
                    var dataId = grid.getStore().getAt(rowIndex).data._id;
                    Ext.Ajax.request({
                        url: 'systemmasterdata/deleteObjectItem',
                        method: 'POST',
                        params: {
                            'masterDataParam': dataId
                        },
                        success: function (response) {
                            var resp = Ext.JSON.decode(response.responseText);
                            if (resp.success) {
                                grid.getStore().removeAt(rowIndex);
                                grid.up('window[name=md-MainWindow]').down('statusbar').setStatus({
                                    text: 'Deleted successfully!',
                                    iconCls: 'x-status-valid',
                                    animate: true
                                });
                                
                            } else {
                                grid.up('window[name=md-MainWindow]').down('statusbar').setStatus({
                                    text: 'Ops! ' + resp.errorMessage,
                                    iconCls: 'x-status-error',
                                    animate: true
                                });
                            }
                            grid.up('window[name=md-MainWindow]').setLoading(false);
                        },
                        failure: function () {
                            grid.setDisabled(true);
                            grid.up('window[name=md-MainWindow]').down('statusbar').setStatus({
                                text: 'Internal Exception Error!',
                                iconCls: 'x-status-error',
                                animate: true
                            });
                        }
                    });
                    
                    
                }
            }
        });
    },
    
    addButtonEvent: function (button) {
        this.createWindow(button);
    },
    
    changePosition: function (ownGrid) {
        var me = this;
        var treePanel = Ext.ComponentQuery.query('[name=md-Tree]')[0];
        var mainWindow = Ext.ComponentQuery.query('[name=md-MainWindow]')[0];
        mainWindow.setLoading(true);
        var dropDatas = [];
        Ext.each(ownGrid.records, function (eachData) {
            dropDatas.push(eachData.data._id);
        });
        if (ownGrid.records[0].data.active === true) {
            me.changeRequest('deactivate', dropDatas, mainWindow, treePanel, ownGrid);
        } else {
            me.changeRequest('activate', dropDatas, mainWindow, treePanel, ownGrid);
        }
    },
    
    loadGrids: function (treePanel, record) {
        /*
        var treePanel = Ext.ComponentQuery.querry('[name=md-Tree')[0];
        var record = treePanel.getSelectionModel().getSelection()[0];   */
        var clickedObject = {
            parent: record.parentNode.raw,
            self: record.raw
        };
        var reqUrl;
        if (clickedObject.parent.type === 'objectType') {
            reqUrl = "objectType=" + clickedObject.self.type;
        }
        if (clickedObject.parent.type !== 'objectType') {
            reqUrl = "objectType=" + clickedObject.parent.type + "&&SID=" + clickedObject.self.type;
        }
        if (treePanel.getSelectionModel().getSelection()[0].raw.leaf) {
            Ext.getStore('AC7.modules.systemmasterdata.app.store.GridAvailableStore').removeAll();
            Ext.getStore('AC7.modules.systemmasterdata.app.store.GridActiveStore').removeAll();
            treePanel.lastSelected = record.raw;
            treePanel.up('window[name=md-MainWindow]').setLoading(true);
            Ext.Ajax.request({
                url: 'systemmasterdata/getObjectItems',
                method: 'POST',
                params: {
                    'masterDataParam': reqUrl
                },
                success: function (response) {
                    var resp = Ext.JSON.decode(response.responseText);
                    if (resp.data) {
                        if (resp.data.responseCode === 'I_OPERATION_SUCCESSFUL') {
                            var activeArray = [], availableArray = [];
                            Ext.each(resp.data.result, function (rec) {
                                if (rec.active === true) {
                                    activeArray.push(rec);
                                } else {
                                    availableArray.push(rec);
                                }
                            });
                            Ext.getStore('AC7.modules.systemmasterdata.app.store.GridActiveStore').add(activeArray);
                            Ext.getStore('AC7.modules.systemmasterdata.app.store.GridAvailableStore').add(availableArray);
                            Ext.ComponentQuery.query('[name=md-ActiveGrid]')[0].setDisabled(false);
                            Ext.ComponentQuery.query('[name=md-AvailableGrid]')[0].setDisabled(false);
                            
                            Ext.defer(function () {
                                treePanel.up('window[name=md-MainWindow]').down('statusbar').setStatus({
                                    text: 'Ready!',
                                    iconCls: 'x-status-valid',
                                    animate: true
                                });
                            }, 2000);
                            
                            
                        } else {
                            treePanel.up('window[name=md-MainWindow]').down('statusbar').setStatus({
                                text: 'Ops! ' + resp.data.responseMessage,
                                iconCls: 'x-status-error',
                                animate: true
                            });
                        }
                        treePanel.up('window[name=md-MainWindow]').setLoading(false);
                    } else {
                        treePanel.up('window[name=md-MainWindow]').down('statusbar').setStatus({
                            text: getText("E_SERVER_ERROR", "Server Error! Please contact system administrator."),
                            iconCls: 'x-status-error',
                            animate: true
                        });
                        treePanel.up('window[name=md-MainWindow]').setLoading(false);
                    }
                    
                },
                failure: function () {
                    treePanel.up('window[name=md-MainWindow]').setLoading(false);
                    treePanel.up('panel[name=md-md-MainPanel]').setDisabled(true);
                    treePanel.up('window').down('statusbar').setStatus({
                        text: 'Internal Exception Error!',
                        iconCls: 'x-status-error',
                        animate: true
                    });
                }
            });
            
        }
    },
    
    changeRequest: function (action, dropDatas, mainWindow, treePanel, ownGrid) {
        var me = this;
        Ext.Ajax.request({
            url: 'systemmasterdata/' + action,
            method: 'POST',
            params: {
                'masterDataParam': JSON.stringify(dropDatas)
            },
            success: function (response) {
                var resp = Ext.JSON.decode(response.responseText);
                if (resp.data) {
                    if (resp.data.responseCode === "I_OPERATION_SUCCESSFUL") {
                        if (ownGrid.records[1]) {
                            mainWindow.down('statusbar').setStatus({
                                text: "Items " + action + "d!",
                                iconCls: 'x-status-valid',
                                animate: true
                            });
                        } else {
                            mainWindow.down('statusbar').setStatus({
                                text: ownGrid.records[0].data.code + " " + action + "d!",
                                iconCls: 'x-status-valid',
                                animate: true
                            });
                        }
                        me.loadGrids(treePanel, treePanel.getSelectionModel().getSelection()[0]);
                    } else {
                        mainWindow.down('statusbar').setStatus({
                            text: "Ops! " + resp.data.responseMessage,
                            iconCls: 'x-status-error',
                            animate: true
                        });
                    }
                    if (ownGrid.records[0].data.objectType === "SID") {
                        treePanel.setLoading(true);
                        Ext.Ajax.request({
                            url: 'systemmasterdata/getObjectTreeItems',
                            method: 'GET',
                            success: function (response1) {
                                var resp1 = Ext.JSON.decode(response1.responseText);
                                if (resp1.data) {
                                    if (resp1.data.responseCode === 'I_OPERATION_SUCCESSFUL') {
                                        mainWindow.setLoading(false);
                                        Ext.getStore('AC7.modules.systemmasterdata.app.store.GridAvailableStore').removeAll();
                                        Ext.getStore('AC7.modules.systemmasterdata.app.store.GridActiveStore').removeAll();
                                        Ext.ComponentQuery.query('[name=md-ActiveGrid]')[0].setDisabled(true);
                                        Ext.ComponentQuery.query('[name=md-AvailableGrid]')[0].setDisabled(true);
                                        var x, y;
                                        var objectArray = [], yObjectArray = [];
                                        var xObjectType = {}, yObjectType = {};
                                        for (x = 0; x < resp1.data.result.length; x++) {
                                            if (resp1.data.result[x].children) {
                                                for (y = 0; y < resp1.data.result[x].children.length; y++) {
                                                    yObjectType = {
                                                        text: resp1.data.result[x].children[y].name,
                                                        type: resp1.data.result[x].children[y].type,
                                                        leaf: true
                                                    };
                                                    yObjectArray.push(yObjectType);
                                                }
                                                xObjectType = {
                                                    text: resp1.data.result[x].name,
                                                    type: resp1.data.result[x].type,
                                                    expanded: true,
                                                    children: yObjectArray
                                                };
                                                objectArray.push(xObjectType);
                                                yObjectArray = [];
                                            } else {
                                                xObjectType = {
                                                    text: resp1.data.result[x].name,
                                                    type: resp1.data.result[x].type,
                                                    leaf: true
                                                };
                                                objectArray.push(xObjectType);
                                            }
                                        }
                                        var tempStore = Ext.create('Ext.data.TreeStore', {
                                            model: 'AC7.modules.systemmasterdata.app.model.TreeModel',
                                            autoload: true,
                                            autoSync: true,
                                            root: {
                                                type: 'objectType',
                                                text: 'Object Types',
                                                children: objectArray
                                            }
                                        });
                                        treePanel.reconfigure(tempStore);
                                        treePanel.up('window[name=md-MainWindow]').down('statusbar').setStatus({
                                            text: 'Ready!',
                                            iconCls: 'x-status-valid',
                                            animate: true
                                        });
                                    } else {
                                        treePanel.up('window[name=md-MainWindow]').down('statusbar').setStatus({
                                            text: 'Ops! ' + resp1.data.responseMessage,
                                            iconCls: 'x-status-error',
                                            animate: true
                                        });
                                    }
                                } else {
                                    treePanel.up('window[name=md-MainWindow]').down('statusbar').setStatus({
                                        text: getText("E_SERVER_ERROR", "Server Error! Please contact system administrator."),
                                        iconCls: 'x-status-error',
                                        animate: true
                                    });
                                    treePanel.setDisabled(true);
                                }
                                treePanel.setLoading(false);
                            },
                            failure: function () {
                                treePanel.setLoading(false);
                                treePanel.setDisabled(true);
                                treePanel.up('window[name=md-MainWindow]').down('statusbar').setStatus({
                                    text: 'Internal Exception Error',
                                    iconCls: 'x-status-error',
                                    animate: true
                                });
                            }
                        });
                    } else {
                        mainWindow.setLoading(false);
                    }
                } else {
                    mainWindow.down('statusbar').setStatus({
                        text: getText("E_SERVER_ERROR", "Server Error! Please contact system administrator."),
                        iconCls: 'x-status-error',
                        animate: true
                    });
                    mainWindow.setLoading(false);
                }
            },
            failure: function () {
                mainWindow.setLoading(false);
                mainWindow.setDisabled(true);
                mainWindow.down('statusbar').setStatus({
                    text: 'Internal Exception Error!',
                    iconCls: 'x-status-error',
                    animate: true
                });
            }
        });
    },
    
    onGridActionColumnEditEvent: function (view, cell, row, col, e) {
        this.createWindow(' ', view, view.getStore().getAt(row), row);
    },
    
    createWindow: function (button, grid, record, index) {
        var selected = Ext.ComponentQuery.query('[name=md-Tree]')[0].getSelectionModel().getSelection()[0];
        if (selected) {
            var that = this;
            var mainPanel = Ext.ComponentQuery.query('[name=md-MainPanel]')[0];
            var availableLanguages = [];
            var createdWindow = Ext.create('Ext.window.Window', {
                name: 'md-PopupWindow',
                id: 'md-PopupWindowId',
                resizeable: false,
                modal: true,
                items: [
                    {
                        xtype: 'form',
                        name: 'md-CreatedWindowPanel',
                        frame: true,
                        bodyPadding: 15,
                        items: [
                            {
                                xtype: 'container',
                                layout: {
                                    type: 'vbox',
                                    align: 'stretch'
                                },
                                items: [
                                    {
                                        xtype: 'container',
                                        layout: {
                                            type: 'hbox',
                                            align: 'stretch'
                                        },
                                        items: [
                                            {
                                                xtype: 'container',
                                                margin: '0 20 0 0',
                                                layout: {
                                                    type: 'vbox',
                                                    align: 'stretch'
                                                },
                                                items: [{
                                                    xtype: 'label',
                                                    style: 'display:inline-block;text-align:center',
                                                    margin: '0 0 5 0',
                                                    text: 'Code'
                                                }, {
                                                    xtype: 'textfield',
                                                    name: 'codeField',
                                                    width: 80,
                                                    disabled: false,
                                                    listeners: {
                                                        change: function (field) {
                                                            if (selected.raw.type === "Language") {
                                                                field.setValue(field.getValue().toLowerCase());
                                                            }
                                                        }
                                                    }
                                                }]
                                            }, {
                                                xtype: 'container',
                                                name: 'md-LangArea',
                                                margin: '0 20 5 0',
                                                layout: {
                                                    type: 'vbox',
                                                    align: 'stretch'
                                                },
                                                items: []
                                            }, {
                                                xtype: 'container',
                                                name: 'md-DescriptionArea',
                                                margin: '0 20 5 0',
                                                layout: {
                                                    type: 'vbox',
                                                    align: 'stretch'
                                                },
                                                items: [],
                                                listeners: {
                                                    afterrender: function (container) {
                                                        container.up('window[name=md-PopupWindow]').setLoading(true);
                                                        var tempField1 = {
                                                            xtype: 'label',
                                                            margin: '0 0 5 0',
                                                            style: 'display:inline-block;text-align:center',
                                                            text: 'Description'
                                                        };
                                                        var tempField0 = {
                                                            xtype: 'label',
                                                            style: 'display:inline-block;text-align:center',
                                                            margin: '0 0 5 0',
                                                            text: 'Lang'
                                                        };
                                                        Ext.ComponentQuery.query('[name=md-LangArea]')[0].add(tempField0);
                                                        Ext.ComponentQuery.query('[name=md-DescriptionArea]')[0].add(tempField1);
                                                        Ext.Ajax.request({
                                                            url: 'systemmasterdata/getAvailableLanguages',
                                                            method: 'GET',
                                                            success: function (response) {
                                                                var resp = Ext.JSON.decode(response.responseText);
                                                                if (resp.data) {
                                                                    if (resp.data.responseCode === "I_OPERATION_SUCCESSFUL") {
                                                                        if (grid) {
                                                                            Ext.each(resp.data.result, function (rec) {
                                                                                availableLanguages.push(rec);
                                                                                tempField1 = {
                                                                                    xtype: 'textfield',
                                                                                    name: rec.valueOf().toLowerCase() + "description",
                                                                                    disabled: true,
                                                                                    width: 200,
                                                                                    listeners: {
                                                                                        beforerender: function (textField) {
                                                                                            if (grid.getStore().getAt(index).data.source === "SYS") {
                                                                                                if (AC7.Globals.hasBooleanPermission("masterdata.sys.edit")) {
                                                                                                    textField.setDisabled(false);
                                                                                                }
                                                                                            } else {
                                                                                                textField.setDisabled(false);
                                                                                            }
                                                                                            
                                                                                        },
                                                                                        afterrender: function (textField) {
                                                                                            Ext.each(grid.getStore().getAt(index).data.names, function (rec2) {
                                                                                                if (rec.valueOf() === rec2.lang) {
                                                                                                    textField.setValue(rec2.name);
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    }
                                                                                };
                                                                                tempField0 = {
                                                                                    xtype: 'textfield',
                                                                                    name: rec.valueOf().toLowerCase() + "field",
                                                                                    disabled: true,
                                                                                    width: 25,
                                                                                    value: rec.valueOf().toLowerCase()
                                                                                };
                                                                                Ext.ComponentQuery.query('[name=md-LangArea]')[0].add(tempField0);
                                                                                Ext.ComponentQuery.query('[name=md-DescriptionArea]')[0].add(tempField1);
                                                                            });
                                                                            
                                                                        } else {
                                                                            
                                                                            Ext.each(resp.data.result, function (rec) {
                                                                                availableLanguages.push(rec);
                                                                                tempField1 = {
                                                                                    xtype: 'textfield',
                                                                                    name: rec.valueOf().toLowerCase() + "description",
                                                                                    disabled: false,
                                                                                    width: 200
                                                                                };
                                                                                tempField0 = {
                                                                                    xtype: 'textfield',
                                                                                    name: rec.valueOf().toLowerCase() + "field",
                                                                                    disabled: true,
                                                                                    width: 35,
                                                                                    value: rec.valueOf().toLowerCase()
                                                                                };
                                                                                Ext.ComponentQuery.query('[name=md-LangArea]')[0].add(tempField0);
                                                                                Ext.ComponentQuery.query('[name=md-DescriptionArea]')[0].add(tempField1);
                                                                            });
                                                                            
                                                                        }
                                                                        container.up('window[name=md-PopupWindow]').setLoading(false);
                                                                    } else {
                                                                        container.up('window[name=md-PopupWindow]').setLoading(false);
                                                                        Ext.ComponentQuery.query('[name=md-MainWindow]')[0].down('statusbar').setStatus({
                                                                            text: 'Ops! Message: ' + resp.data.responseMessage,
                                                                            iconCls: 'x-status-error',
                                                                            animate: true
                                                                        });
                                                                    }
                                                                } else {
                                                                    Ext.ComponentQuery.query('[name=md-MainWindow]')[0].down('statusbar').setStatus({
                                                                        text: 'Internal Exception Error!',
                                                                        iconCls: 'x-status-error',
                                                                        animate: true
                                                                    });
                                                                    container.up('window[name=md-PopupWindow]').setLoading(false);
                                                                    container.up('window[name=md-CreatedWindowPanel]').setDisabled(true);
                                                                }
                                                            },
                                                            failure: function (response) {
                                                                Ext.ComponentQuery.query('[name=md-MainWindow]')[0].down('statusbar').setStatus({
                                                                    text: 'Internal Exception Error!',
                                                                    iconCls: 'x-status-error',
                                                                    animate: true
                                                                });
                                                                container.up('window[name=md-PopupWindow]').setLoading(false);
                                                                container.up('window[name=md-CreatedWindowPanel]').setDisabled(true);
                                                            }
                                                        });
                                                        
                                                    }
                                                }
                                            }
                                        ]
                                    }, {
                                        xtype: 'container',
                                        layout: 'hbox',
                                        items: [
                                            {
                                                xtype: 'label',
                                                margin: '5 0 0 5',
                                                style: 'display:inline-block;text-align:center',
                                                text: 'Remark:',
                                                flex: 1
                                            }, {
                                                xtype: 'container',
                                                flex: 1
                                            }, {
                                                xtype: 'textfield',
                                                name: 'mdRemark',
                                                listeners: {
                                                    afterrender: function (textField) {
                                                        if (grid) {
                                                            textField.setValue(grid.getStore().getAt(index).data.remark)
                                                        }
                                                    }
                                                },
                                                flex: 5
                                            }
                                        ]
                                    }/*
                                    {
                                        xtype: 'textfield',
                                        name: 'mdRemark',
                                        fieldLabel: 'Remark',
                                        labelPad: -5,
                                        listeners: {
                                            afterrender: function (textField) {
                                                if (grid) {
                                                    textField.setValue(grid.getStore().getAt(index).data.remark)
                                                }
                                            }
                                        }
                                    }*/
                                ]
                            }]
                    }
                ],
                dockedItems: {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    items: [
                        {
                            xtype: 'container',
                            flex: 1
                        }, {
                            xtype: 'container',
                            items: {
                                xtype: 'button',
                                name: 'md-PopupSave',
                                iconCls: 'icon-systemmasterdata-save',
                                text: 'Save',
                                listeners: {
                                    click: function (button) {
                                        that.savePopupWindow(Ext.ComponentQuery.query('[name=md-CreatedWindowPanel]')[0].getForm(), grid, index, availableLanguages, button);
                                    }
                                }
                            }
                        }, {
                            xtype: 'container',
                            items: {
                                xtype: 'button',
                                name: 'md-PopupClose',
                                text: 'Close',
                                listeners: {
                                    click: function (button) {
                                        button.up('window[name=md-PopupWindow]').close();
                                    }
                                }
                            }
                        }
                    ]
                },
                listeners: {
                    beforeclose: function () {
                        mainPanel.setDisabled(false);
                        Ext.ComponentQuery.query('[name=md-MainWindow]')[0].setLoading(false);
                    }
                }
            });
            
            if (button === ' ') {
                mainPanel.setDisabled(true);
                Ext.ComponentQuery.query('[name=codeField]')[0].setValue(grid.getStore().getAt(index).data.code);
                Ext.ComponentQuery.query('[name=codeField]')[0].setDisabled(true);
                createdWindow.setTitle('Update');
                
            } else {
                mainPanel.setDisabled(true);
                createdWindow.setTitle('Add');
            }
            createdWindow.show();
        } else {
            Ext.Msg.show({
                title: 'WARNING',
                msg: 'Please select an ObjectType to Add new data!',
                icon: Ext.Msg.WARNING
            });
        }
        
    },
    savePopupWindow: function (popupForm, popGrid, popIndex, languages, button) {
        var mainWindow = Ext.ComponentQuery.query('[name=md-MainWindow]')[0];
        Ext.ComponentQuery.query('[name=md-MainWindow]')[0].setLoading(true);
        var temp = {};
        var tempNames = {};
        var formValue = popupForm.getValues();
        var formValid = false;
        for (var cnt = 0; cnt < languages.length; cnt++) {
            if (formValue[languages[cnt] + "description"] !== "") {
                formValid = true;
            }
        }
        if (languages.length === 0) {
            formValid = true;
        }
        if (popGrid) {
            if (formValid) {
                var storeData = popGrid.getStore().getAt(popIndex).data;
                if (AC7.Globals.hasBooleanPermission("masterdata.sys.edit")) {
                    temp = {
                        SID: storeData.SID,
                        _id: storeData._id,
                        active: storeData.active,
                        code: storeData.code,
                        names: [],
                        objectType: storeData.objectType,
                        remark: Ext.ComponentQuery.query('[name=mdRemark]')[0].getValue(),
                        source: 'SYS'
                    };
                } else {
                    temp = {
                        SID: storeData.SID,
                        _id: storeData._id,
                        active: storeData.active,
                        code: storeData.code,
                        names: [],
                        objectType: storeData.objectType,
                        remark: Ext.ComponentQuery.query('[name=mdRemark]')[0].getValue(),
                        source: 'USER'
                    };
                }
                
                tempNames = {};
                Ext.each(languages, function (rec) {
                    var strField = rec.valueOf() + "field";
                    var strDesc = rec.valueOf() + "description";
                    if (Ext.ComponentQuery.query('[name=' + strDesc + ']')[0].getValue() !== "") {
                        tempNames = {
                            'lang': Ext.ComponentQuery.query('[name=' + strField + ']')[0].getValue(),
                            'name': Ext.ComponentQuery.query('[name=' + strDesc + ']')[0].getValue()
                        };
                        temp.names.push(tempNames);
                    }
                });
                popGrid.getStore().removeAt(popIndex);
                if (temp.active) {
                    Ext.getStore('AC7.modules.systemmasterdata.app.store.GridActiveStore').add(temp);
                } else {
                    Ext.getStore('AC7.modules.systemmasterdata.app.store.GridAvailableStore').add(temp);
                }
                Ext.Ajax.request({
                    url: 'systemmasterdata/updateObject',
                    method: 'POST',
                    params: {
                        'masterDataParam': JSON.stringify(temp)
                    },
                    success: function (response) {
                        var resp = Ext.JSON.decode(response.responseText);
                        if (resp.success) {
                            mainWindow.down('statusbar').setStatus({
                                text: 'Updated successfully!',
                                iconCls: 'x-status-valid',
                                animate: true
                            });
                        } else {
                            mainWindow.down('statusbar').setStatus({
                                text: "Ops! " + resp.errorMessage,
                                iconCls: 'x-status-error',
                                animate: true
                            });
                        }
                        mainWindow.setLoading(false);
                    },
                    failure: function () {
                        mainWindow.setLoading(false);
                        mainWindow.setDisabled(true);
                        mainWindow.down('statusbar').setStatus({
                            text: 'UnExcepted Error! Please contact system administrator.',
                            iconCls: 'x-status-error',
                            animate: true
                        });
                    }
                });
                
                button.up('window[name=md-PopupWindow]').close();
            } else {
                Ext.Msg.show({
                    title: 'WARNING',
                    msg: 'You must type at least one \'Description\'',
                    icon: Ext.Msg.WARNING
                });
            }
        } else {
            if (formValue.codeField !== "") {
                if (formValid) {
                    
                    var objectType = {
                        parent: Ext.ComponentQuery.query('[name=md-Tree]')[0].getSelectionModel().getSelection()[0].parentNode.raw,
                        self: Ext.ComponentQuery.query('[name=md-Tree]')[0].getSelectionModel().getSelection()[0].raw
                    };
                    
                    if (objectType.parent.type === "objectType") {
                        objectType.parent = objectType.self;
                        objectType.self = '';
                    }
                    if (AC7.Globals.hasBooleanPermission("masterdata.sys.edit")) {
                        temp = {
                            SID: objectType.self === "" ? null : objectType.self.type,
                            active: true,
                            code: Ext.ComponentQuery.query('[name=codeField]')[0].getValue(),
                            names: [],
                            objectType: objectType.parent.type,
                            remark: Ext.ComponentQuery.query('[name=mdRemark]')[0].getValue(),
                            source: 'SYS'
                        };
                    } else {
                        temp = {
                            SID: objectType.self === "" ? null : objectType.self.type,
                            active: true,
                            code: Ext.ComponentQuery.query('[name=codeField]')[0].getValue(),
                            names: [],
                            objectType: objectType.parent.type,
                            remark: Ext.ComponentQuery.query('[name=mdRemark]')[0].getValue(),
                            source: 'USER'
                        };
                    }
                    tempNames = {};
                    
                    Ext.each(languages, function (rec) {
                        var strField = rec.valueOf() + "field";
                        var strDesc = rec.valueOf() + "description";
                        if (Ext.ComponentQuery.query('[name=' + strDesc + ']')[0].getValue() !== "") {
                            tempNames = {
                                'lang': Ext.ComponentQuery.query('[name=' + strField + ']')[0].getValue(),
                                'name': Ext.ComponentQuery.query('[name=' + strDesc + ']')[0].getValue()
                            };
                            temp.names.push(tempNames);
                        }
                        
                    });
                    
                    Ext.Ajax.request({
                        url: 'systemmasterdata/saveObject',
                        method: 'POST',
                        params: {
                            'masterDataParam': JSON.stringify(temp)
                        },
                        success: function (response) {
                            var resp = Ext.JSON.decode(response.responseText);
                            if (resp.success) {
                                Ext.getStore('AC7.modules.systemmasterdata.app.store.GridActiveStore').add(resp.data.result);
                                mainWindow.down('statusbar').setStatus({
                                    text: 'Added successfully!',
                                    iconCls: 'x-status-valid',
                                    animate: true
                                });
                            } else {
                                mainWindow.down('statusbar').setStatus({
                                    text: "Ops! " + resp.errorMessage,
                                    iconCls: 'x-status-error',
                                    animate: true
                                });
                            }
                            mainWindow.setLoading(false);
                        },
                        failure: function () {
                            mainWindow.setLoading(false);
                            mainWindow.setDisabled(true);
                            mainWindow.down('statusbar').setStatus({
                                text: 'UnExcepted Error! Please contact system administrator.',
                                iconCls: 'x-status-error',
                                animate: true
                            });
                        }
                    });
                    button.up('window[name=md-PopupWindow]').close();
                } else {
                    Ext.Msg.show({
                        title: 'WARNING',
                        msg: 'You must type at least one \'Name\'',
                        icon: Ext.Msg.WARNING
                    });
                }
                
            } else {
                Ext.Msg.show({
                    title: 'WARNING',
                    msg: 'You must type \'Code\'',
                    icon: Ext.Msg.WARNING
                });
            }
            
        }
    },
    
    importCsvField: function () {
        var me = this;
        // True if the detected browser is Internet Explorer 9.x or lower.
        if (Ext.isIE9m) {
            Ext.Msg.show({
                title: 'ERROR',
                msg: 'Config IMPORT is not supported for Internet Explorer 9.x or lower.',
                buttons: Ext.Msg.OK,
                icon: Ext.Msg.WARNING
            });
            
        } else {
            var f = Ext.ComponentQuery.query('filefield[name=md-FileUpload]')[0].getEl().down('input[type=file]').dom.files[0];
            var sb = Ext.ComponentQuery.query('window[name=md-MainWindow]')[0].down('statusbar');
            var r = new FileReader();
            if (!f) {
                sb.setStatus({
                    text: 'Oops! Failed to load file!',
                    iconCls: 'x-status-error',
                    animate: true
                });
                
            }
            else if (f.type === 'csv') {
                sb.setStatus({
                    text: 'Oops! ' + f.name + ' is not a csv file!',
                    iconCls: 'x-status-error',
                    animate: true
                });
            }
            /*
            else if (f.size > 1024 * 1024) {
                sb.setStatus({
                    text: 'Oops!' + f.name + ' file size must be max 1MB!',
                    iconCls: 'x-status-error',
                    animate: true
                });
                
            }*/
            else {
                r.readAsText(f);
            }
            r.onload = function (e) {
                
                var rows = e.target.result.split("\n");
                var xCnt, element;
                var items = [], item = {}, tempNames = [], tempName = {};
                Ext.each(rows, function (rec, index) {
                    if (index !== 0 && rec !== "") {
                        element = rec.split(",");
                        for (xCnt = 4; xCnt < element.length - 1; xCnt++) {
                            tempName = {
                                lang: element[xCnt].split('-')[0],
                                name: element[xCnt].split('-')[1]
                            };
                            tempNames.push(tempName);
                            tempName = {};
                        }
                        item = {
                            objectType: element[0],
                            active: element[1],
                            SID: element[2],
                            code: element[3],
                            source: 'SYS',
                            names: tempNames
                        };
                        items.push(item);
                        item = {};
                        tempNames = [];
                    }
                });
                Ext.Ajax.request({
                    url: 'systemmasterdata/testUploadCsvFile',
                    method: 'POST',
                    params: {
                        'masterDataParam': JSON.stringify(items)
                    },
                    success: function (response) {
                        var resp = Ext.JSON.decode(response.responseText);
                        if (resp.success) {
                            console.log(resp);
                            if (resp.data.result.length !== 0) {
                                var errorDatas = [],
                                    uploadError = {};
                                Ext.each(resp.data.result, function (rec) {
                                    if (rec.errors.length !== 0) {
                                        Ext.each(rec.errors, function (rec2) {
                                            errorDatas.push(rec);
                                        });
                                    } else {
                                        if (rec.warnings.length > 0) {
                                            Ext.each(rec.warnings, function (recWarning) {
                                                rec["warningStatus"] = true;
                                                rec["warningMessage"] = recWarning.warningMessage;
                                                rec["originalData"] = recWarning.masterDataObject;
                                            });
                                        }
                                    }
                                });
                                
                                if (errorDatas.length !== 0) {
                                    var errorMessages = "";
                                    Ext.each(errorDatas, function (errorData, errorIndex) {
                                        errorMessages += "Missing mandatory on " + errorIndex + ". column!" + "<br />";
                                        Ext.each(errorData.errors, function (error) {
                                            errorMessages += error.errorMessage + "<br />"
                                        });
                                    });
                                    Ext.Msg.show({
                                        title: 'Upload ' + f.name + 'file Error',
                                        //msg: f.name + ' File was corrupted! <br />' + 'null exception!',
                                        msg: errorMessages,
                                        icon: Ext.Msg.ERROR
                                    });
                                } else {
                                    Ext.getStore('AC7.modules.systemmasterdata.app.store.GridWarningStore').removeAll();
                                    Ext.getStore('AC7.modules.systemmasterdata.app.store.GridWarningStore').add(resp.data.result);
                                    var gridSelectionModel = Ext.create('Ext.selection.CheckboxModel');
                                    var warningWindow = Ext.create('Ext.window.Window', {
                                        title: 'Warnings',
                                        name: 'md-UploadWarningWindow',
                                        resizeable: false,
                                        modal: true,
                                        width: '60%',
                                        height: '60%',
                                        layout: 'fit',
                                        items: [
                                            {
                                                xtype: 'grid',
                                                layout: 'fit',
                                                name: 'md-UploadWarningGrid',
                                                anchor: '100%',
                                                store: 'AC7.modules.systemmasterdata.app.store.GridWarningStore',
                                                cls: 'systemmasterdata-grid',
                                                //selModel: gridSelectionModel,
                                                requires: [
                                                    'Ext.ux.RowExpander'
                                                ],
                                                emptyText: 'There are not any Active Data',
                                                flex: 1,
                                                columns: [
                                                    {
                                                        text: 'Object Type',
                                                        dataIndex: 'objectType',
                                                        flex: 2
                                                    },
                                                    {
                                                        text: 'SID',
                                                        dataIndex: 'SID',
                                                        flex: 1
                                                    },
                                                    {
                                                        text: 'Code',
                                                        dataIndex: 'code',
                                                        flex: 1
                                                    },
                                                    {
                                                        text: 'Lang',
                                                        flex: 1,
                                                        renderer: function (value, metaData, record) {
                                                            
                                                            var name = _.findWhere(record.get('names'), {lang: AC7.Globals.clientConfig.user.selectedLang});
                                                            if (name) {
                                                                return name.lang;
                                                            }
                                                            
                                                            var nameEn = _.findWhere(record.get('names'), {lang: 'en'});
                                                            if (nameEn) {
                                                                return nameEn.lang;
                                                            }
                                                            
                                                            if (!Ext.isEmpty(record.get('names'))) {
                                                                return record.get('names')[0].lang;
                                                            }
                                                            return '';
                                                        }
                                                    },
                                                    {
                                                        text: 'Description',
                                                        flex: 4,
                                                        renderer: function (value, metaData, record) {
                                                            
                                                            var nameObj = _.findWhere(record.get('names'), {lang: AC7.Globals.clientConfig.user.selectedLang});
                                                            if (nameObj) {
                                                                return nameObj.name;
                                                            }
                                                            
                                                            var nameEn = _.findWhere(record.get('names'), {lang: 'en'});
                                                            if (nameEn) {
                                                                return nameEn.name;
                                                            }
                                                            
                                                            if (!Ext.isEmpty(record.get('names'))) {
                                                                return record.get('names')[0].name;
                                                            }
                                                            return '';
                                                        }
                                                    }, {
                                                        text: 'Warning',
                                                        flex: 5,
                                                        renderer: function (value, metaData, record) {
                                                            
                                                            if (!Ext.isEmpty(record.get("warningMessage"))) {
                                                                metaData.tdAttr = 'data-qtip="' + record.get("warningMessage") + '"';
                                                                return record.get("warningMessage");
                                                            }
                                                            return 'Ready to save';
                                                        }
                                                    }, {
                                                        text: 'isActive',
                                                        dataIndex: 'active',
                                                        flex: 1
                                                    }
                                                ],
                                                plugins: [
                                                    {
                                                        ptype: 'rowexpander',
                                                        pluginId: 'md-GridRowExpander',
                                                        
                                                        rowBodyTpl: new Ext.XTemplate(
                                                            '<hr />',
                                                            'New Values',
                                                            '<dl class = "systemmasterdata-grid-rowexpander">',
                                                            '<tpl for="names">',
                                                            '<dt>{lang}- {name}</dt>',
                                                            '</tpl>',
                                                            "<tpl if='this.isEmpty(remark)'>",
                                                            '<dt>Remark: {remark}</dt>',
                                                            "</tpl>",
                                                            '</dl>',
                                                            "<tpl if='this.isEmpty(warningStatus)'>",
                                                            '<hr />',
                                                            'Old Values',
                                                            '<dl class = "systemmasterdata-grid-warningAlready">',
                                                            '<tpl for="originalData.names">',
                                                            '<dt>{lang}- {name}</dt>',
                                                            '</tpl>',
                                                            "<tpl if='this.isEmpty(originalData.remark)'>",
                                                            '<dt>Remark: {originalData.remark}</dt>',
                                                            "</tpl>",
                                                            '</dl>',
                                                            '</tpl>',
                                                            {
                                                                isEmpty: function (data) {
                                                                    if (!Ext.isEmpty(data)) {
                                                                        return true;
                                                                    } else {
                                                                        return false;
                                                                    }
                                                                }
                                                            }),
                                                        /*
                                                        rowBodyTpl: new Ext.XTemplate(
                                                            '<dl class = "systemmasterdata-grid-rowexpander">',
                                                            '<tpl for="originalData.names">',
                                                            '<dt>{originalData.lang}- {originalData.name}</dt>',
                                                            '</tpl>',
                                                            "<tpl if='remark != \"\"'>",
                                                            '<dt>Remark: {remark}</dt>',
                                                            "</tpl>",
                                                            '</dl>'),
                                                        */
                                                        expandRow: function (rowIdx) {
                                                            var rowNode = this.view.getNode(rowIdx),
                                                                row = Ext.get(rowNode),
                                                                nextBd = Ext.get(row).down(this.rowBodyTrSelector),
                                                                record = this.view.getRecord(rowNode),
                                                                grid = this.getCmp();
                                                            if (row.hasCls(this.rowCollapsedCls)) {
                                                                row.removeCls(this.rowCollapsedCls);
                                                                nextBd.removeCls(this.rowBodyHiddenCls);
                                                                this.recordsExpanded[record.internalId] = true;
                                                                this.view.fireEvent('expandbody', rowNode, record, nextBd.dom);
                                                            }
                                                        },
                                                        
                                                        collapseRow: function (rowIdx) {
                                                            var rowNode = this.view.getNode(rowIdx),
                                                                row = Ext.get(rowNode),
                                                                nextBd = Ext.get(row).down(this.rowBodyTrSelector),
                                                                record = this.view.getRecord(rowNode),
                                                                grid = this.getCmp();
                                                            if (!row.hasCls(this.rowCollapsedCls)) {
                                                                row.addCls(this.rowCollapsedCls);
                                                                nextBd.addCls(this.rowBodyHiddenCls);
                                                                this.recordsExpanded[record.internalId] = false;
                                                                this.view.fireEvent('collapsebody', rowNode, record, nextBd.dom);
                                                            }
                                                        }
                                                    }
                                                ],
                                                viewConfig: {
                                                    getRowClass: function (record, rowIndex, rowParams, store) {
                                                        if (record.get('warningStatus') === true) {
                                                            return 'md-customGridRowHighlightWarning';
                                                        } else {
                                                            return 'md-customGridRowHighlightSuccess';
                                                        }
                                                    }
                                                },
                                                listeners: {
                                                    //   https://fiddle.sencha.com/#fiddle/10bt
                                                }
                                            }
                                        ],
                                        
                                        dockedItems: [
                                            {
                                                xtype: 'toolbar',
                                                dock: 'top',
                                                items: [{
                                                    xtype: 'container',
                                                    flex: 1
                                                }, {
                                                    xtype: 'container',
                                                    items: [{
                                                        xtype: 'button',
                                                        name: 'md-UploadExpand',
                                                        iconCls: 'icon-systemmasterdata-expand',
                                                        listeners: {
                                                            click: function (button) {
                                                                var rowExpander = Ext.ComponentQuery.query('grid[name=md-UploadWarningGrid]')[0].getPlugin("md-GridRowExpander");
                                                                var nodes = rowExpander.view.getNodes();
                                                                for (var i = 0; i < nodes.length; i++) {
                                                                    rowExpander.expandRow(i);
                                                                }
                                                            }
                                                        }
                                                    }, {
                                                        xtype: 'button',
                                                        name: 'md-UploadCollapse',
                                                        iconCls: 'icon-systemmasterdata-collapse',
                                                        listeners: {
                                                            
                                                            click: function (button) {
                                                                var rowExpander = Ext.ComponentQuery.query('grid[name=md-UploadWarningGrid]')[0].getPlugin("md-GridRowExpander");
                                                                var nodes = rowExpander.view.getNodes();
                                                                for (var i = 0; i < nodes.length; i++) {
                                                                    rowExpander.collapseRow(i);
                                                                }
                                                            }
                                                            
                                                        }
                                                    }]
                                                }]
                                            },
                                            {
                                                xtype: 'toolbar',
                                                dock: 'bottom',
                                                items: [
                                                    {
                                                        xtype: 'container',
                                                        flex: 1
                                                    }, {
                                                        xtype: 'container',
                                                        items: {
                                                            xtype: 'button',
                                                            text: 'Save',
                                                            iconCls: 'icon-systemmasterdata-save',
                                                            name: 'md-WarningSaveButton',
                                                            listeners: {
                                                                click: function (button) {
                                                                    var mainWindow = Ext.ComponentQuery.query('window[name=md-MainWindow]')[0];
                                                                    mainWindow.setLoading(true);
                                                                    Ext.Ajax.request({
                                                                        url: 'systemmasterdata/uploadCsvFile',
                                                                        method: 'POST',
                                                                        params: {
                                                                            'masterDataParam': JSON.stringify(items)
                                                                        },
                                                                        success: function (response2) {
                                                                            var resp2 = Ext.JSON.decode(response2.responseText);
                                                                            if (resp2.success) {
                                                                                if (resp2.data) {
                                                                                    if (resp2.data.responseCode === "I_OPERATION_SUCCESSFUL") {
                                                                                        mainWindow.down('statusbar').setStatus({
                                                                                            text: 'Upload successful!',
                                                                                            iconCls: 'x-status-valid',
                                                                                            animate: true
                                                                                        });
                                                                                        mainWindow.setLoading(false);
                                                                                        button.up('window[name=md-UploadWarningWindow]').close();
                                                                                        me.treeViewRefresh(mainWindow.down('panel[name=md-Tree]'));
                                                                                        
                                                                                    } else {
                                                                                        mainWindow.down('statusbar').setStatus({
                                                                                            text: 'Ops! ' + resp2.data.responseMessage,
                                                                                            iconCls: 'x-status-error',
                                                                                            animate: true
                                                                                        });
                                                                                        mainWindow.setLoading(false);
                                                                                        button.up('window[name=md-UploadWarningWindow]').close();
                                                                                    }
                                                                                } else {
                                                                                    mainWindow.down('statusbar').setStatus({
                                                                                        text: 'Unexception error!',
                                                                                        iconCls: 'x-status-error',
                                                                                        animate: true
                                                                                    });
                                                                                    mainWindow.setLoading(false);
                                                                                    button.up('window[name=md-UploadWarningWindow]').close();
                                                                                }
                                                                            }
                                                                            else {
                                                                                mainWindow.down('statusbar').setStatus({
                                                                                    text: 'Internal Server Error!',
                                                                                    iconCls: 'x-status-error',
                                                                                    animate: true
                                                                                });
                                                                                mainWindow.setLoading(false);
                                                                                button.up('window[name=md-UploadWarningWindow]').close();
                                                                            }
                                                                        },
                                                                        failure: function () {
                                                                            mainWindow.down('statusbar').setStatus({
                                                                                text: 'Internal Server Error!',
                                                                                iconCls: 'x-status-error',
                                                                                animate: true
                                                                            });
                                                                            mainWindow.setLoading(false);
                                                                            button.up('window[name=md-UploadWarningWindow]').close();
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        }
                                                    }, {
                                                        xtype: 'container',
                                                        items: {
                                                            xtype: 'button',
                                                            text: 'close',
                                                            listeners: {
                                                                click: function (button) {
                                                                    button.up('window[name=md-UploadWarningWindow]').close();
                                                                }
                                                            }
                                                        }
                                                    }
                                                ]
                                            }
                                        ]
                                    });
                                    warningWindow.show();
                                }
                            }
                            
                        }
                        else {
                            Ext.ComponentQuery.query('[name=md-MainWindow]')[0].down('statusbar').setStatus({
                                text: 'Ops! Message: ' + resp.errorMessage,
                                iconCls: 'x-status-error',
                                animate: true
                            })
                        }
                        
                    },
                    failure: function () {
                        Ext.ComponentQuery.query('[name=md-MainWindow]')[0].down('statusbar').setStatus({
                            text: getText("E_SERVER_ERROR", "Server Error! Please contact system administrator."),
                            iconCls: 'x-status-error',
                            animate: true
                        });
                    }
                });
            }
        }
    }
});