/**
 *   Oray KURT
 *   15-May-2018
 *   systemmasterdata.app.model
 */

Ext.define('AC7.modules.systemmasterdata.app.model.TreeModel',{
    extend: 'Ext.data.Model',
    fields: [
        'text'
    ],
    hasMany: {
        model: 'Children', name: 'children'
    }
    
    /*
    proxy: {
        type: 'rest',
        url: '../SampleConfigData.json',
        reader: {
            type: 'json',
            root: 'AC7.modules.systemmasterdata.app.model.TreeModel'
        }
    }
    */
});

Ext.define('Children',{
   extend: 'Ext.data.Model',
    fields: [
        'text'
    ]
});