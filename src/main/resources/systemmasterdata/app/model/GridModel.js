/**
 *   Oray KURT
 *   16-May-2018
 *   systemmasterdata.app.model
 */

Ext.define('AC7.modules.systemmasterdata.app.model.GridModel',{
    extend: 'Ext.data.Model',
    fields: [
        'SID',
        '_id',
        'active',
        'code',
        'objectType',
        'remark',
        'source',
        'warningMessage',
        'warningStatus',
        'originalData',
        { name: 'names', defaultValue : [] }
    ]
});