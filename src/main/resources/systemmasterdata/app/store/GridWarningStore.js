/**
 *   Oray KURT
 *   19-Jun-2018
 *   systemmasterdata.app.store
 */

Ext.define('AC7.modules.systemmasterdata.app.store.GridWarningStore', {
    extend: 'Ext.data.Store',
    model: 'AC7.modules.systemmasterdata.app.model.GridModel',
    storeId: 'md-GridWarningStoreId',
    data: []
});