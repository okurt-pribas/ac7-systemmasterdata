/**
 *   Oray KURT
 *   17-May-2018
 *   systemmasterdata.app.store
 */

Ext.define('AC7.modules.systemmasterdata.app.store.GridAvailableStore', {
    extend: 'Ext.data.Store',
    model: 'AC7.modules.systemmasterdata.app.model.GridModel',
    storeId: 'md-GridAvailableStoreId',
    data: []
});