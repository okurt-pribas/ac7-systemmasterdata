/**
 *   Oray KURT
 *   16-May-2018
 *   systemmasterdata.app.store
 */


Ext.define('AC7.modules.systemmasterdata.app.store.GridStore', {
    extend: 'Ext.data.Store',
    model: 'AC7.modules.systemmasterdata.app.model.GridModel',
    autoLoad: true,
    data: []
});
