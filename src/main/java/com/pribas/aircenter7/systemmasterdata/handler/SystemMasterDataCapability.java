package com.pribas.aircenter7.systemmasterdata.handler;

import javax.servlet.http.HttpServletResponse; /**
 * Oray KURT
 * 16-May-2018
 * com.pribas.aircenter7.systemmasterdata.handler
 */
public interface SystemMasterDataCapability {

    String getObjectTypes() throws Exception;

    String getObjectItems(String attribute) throws Exception;

    String updateObjectItem(String updateObjectItemStr) throws Exception;

    String saveObject(String saveObjectItemStr) throws Exception;

    String saveObjects(String saveObjectItemStr) throws Exception;

    String deleteObject(String deleteObjectId) throws Exception;

    String getAvailableLanguages() throws Exception;

    void getCsvFile(String masterDataParam, HttpServletResponse httpServletResponse, String objectType) throws Exception;

    String testObjectItemsToImport(String csvObjects) throws Exception;

    String objectItemsToImport(String csvObjects) throws Exception;

    String getObjectItemsWithoutFilter() throws Exception;

    String deactiveItems(String objectItems) throws Exception;

    String activateItems(String objectItems) throws Exception;

}
