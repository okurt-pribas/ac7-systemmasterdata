package com.pribas.aircenter7.systemmasterdata.handler;

import com.pribas.aircenter7.systemmasterdata.utils.CsvFileUtil;
import com.pribas.aircenter7.web.core.service.http.HttpServiceCapability;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;

/**
 * Oray KURT
 * 16-May-2018
 * com.pribas.aircenter7.systemmasterdata.handler
 */

@Service
public class SystemMasterDataHandler implements SystemMasterDataCapability{


    public final static String MASTERDATA_SERVICE_NAME = "systemmasterdata";

    @Autowired
    private HttpServiceCapability httpServiceCapability;

    @Override
    public String getObjectTypes() throws Exception {
        return httpServiceCapability.get(MASTERDATA_SERVICE_NAME, "/v1/objectTypes");
    }

    @Override
    public String getObjectItems(String attribute) throws Exception {
        return httpServiceCapability.post(MASTERDATA_SERVICE_NAME,"/v1/masterDataObjects/filter?" + attribute,"");
    }

    @Override
    public String getObjectItemsWithoutFilter() throws Exception {
        return httpServiceCapability.get(MASTERDATA_SERVICE_NAME,"/v1/masterDataObjects");
    }

    @Override
    public String deactiveItems(String objectItems) throws Exception {
        return httpServiceCapability.put(MASTERDATA_SERVICE_NAME, "/v1/masterDataObjects/deactivate",objectItems);
    }

    @Override
    public String activateItems(String objectItems) throws Exception {
        return httpServiceCapability.put(MASTERDATA_SERVICE_NAME, "/v1/masterDataObjects/activate",objectItems);
    }

    @Override
    public String updateObjectItem(String updateObjectItemStr) throws Exception {
        return httpServiceCapability.put(MASTERDATA_SERVICE_NAME,"/v1/masterDataObject",updateObjectItemStr);
    }

    @Override
    public String saveObject(String saveObjectItemStr) throws Exception {
        return httpServiceCapability.post(MASTERDATA_SERVICE_NAME, "/v1/masterDataObject", saveObjectItemStr );
    }

    @Override
    public String saveObjects(String saveObjectItemStr) throws Exception {
        return httpServiceCapability.post(MASTERDATA_SERVICE_NAME, "/v1/masterDataObjects", saveObjectItemStr );
    }

    @Override
    public String deleteObject(String deleteObjectId) throws Exception {
        return httpServiceCapability.delete(MASTERDATA_SERVICE_NAME, "/v1/masterDataObject/" + deleteObjectId,null, "UTF-8");
    }

    @Override
    public String getAvailableLanguages() throws Exception{
        return httpServiceCapability.get(MASTERDATA_SERVICE_NAME, "/v1/availableLanguages");
    }

    @Override
    public void getCsvFile(String masterDataParam, HttpServletResponse response, String objectType) throws Exception {
        CsvFileUtil csvFileUtil = new CsvFileUtil();
        csvFileUtil.getCsvFile(masterDataParam, response, objectType);
    }

    @Override
    public String testObjectItemsToImport(String csvObjects) throws Exception{
        return httpServiceCapability.post(MASTERDATA_SERVICE_NAME,"/v1/masterDataObjects?test=true", csvObjects);
    }

    @Override
    public String objectItemsToImport(String csvObjects) throws Exception{
        return httpServiceCapability.post(MASTERDATA_SERVICE_NAME,"/v1/masterDataObjects", csvObjects);
    }

}
