package com.pribas.aircenter7.systemmasterdata.utils;

import com.google.gson.Gson;
import com.pribas.aircenter7.systemmasterdata.model.ObjectItemModel;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Oray KURT
 * 26-May-2018
 * com.pribas.aircenter7.systemmasterdata.utils
 */
public class CsvFileUtil {

    private static final String SEPARATOR = ",";

    public void getCsvFile(String masterDataParam, HttpServletResponse response, String objectType) throws IOException {


        ObjectItemModel data[] = new Gson().fromJson(masterDataParam, ObjectItemModel[].class);
        StringBuilder oneLine;
        StringBuilder test;
        String fileObject = "";
        test = new StringBuilder("objectType,active,SID,code,lang-names\n");
        for (ObjectItemModel aData : data) {
            oneLine = new StringBuilder();
            oneLine.append(aData.getObjectType());
            oneLine.append(SEPARATOR);
            oneLine.append(aData.getActive());
            oneLine.append(SEPARATOR);
            oneLine.append(aData.getSID());
            oneLine.append(SEPARATOR);
            oneLine.append(aData.getCode());
            oneLine.append(SEPARATOR);
            for (int j = 0; j < aData.getNames().size(); j++) {
                oneLine.append(aData.getNames().get(j).getLang());
                oneLine.append("-");
                oneLine.append(aData.getNames().get(j).getName());
                oneLine.append(SEPARATOR);
            }
            test.append(oneLine.toString()).append("\n");
        }
        byte [] bytes = test.toString().getBytes();
        response.setHeader( "Cache-Control", "private" );
        response.setHeader( "Pragma", "cache" );
        response.setHeader( "Content-Type", "application/octet-stream" );
        response.setHeader( "Content-Length", bytes.length + "" );
        response.setHeader( "Content-Disposition", "attachment; filename=SystemMasterData_" + objectType + ".csv");
        response.getOutputStream().write(bytes, 0, bytes.length);

    }
}

