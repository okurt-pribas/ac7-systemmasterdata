package com.pribas.aircenter7.systemmasterdata.model;

import java.util.List;

/**
 * Oray KURT
 * 30-May-2018
 * com.pribas.aircenter7.systemmasterdata.model
 */
public class CsvResponse {
    private List<GetObjectItems> result;

    public List<GetObjectItems> getResult() {
        return result;
    }

    public void setResult(List<GetObjectItems> result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "CsvResponse{" +
                "result=" + result +
                '}';
    }
}
