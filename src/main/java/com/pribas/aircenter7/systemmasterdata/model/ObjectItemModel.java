package com.pribas.aircenter7.systemmasterdata.model;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Oray KURT
 * 26-May-2018
 * com.pribas.aircenter7.systemmasterdata.model
 */
public class ObjectItemModel {
    private String SID;
    private String _id;
    private String active;
    private String code;
    private ArrayList<NamesObject> names;
    private String objectType;
    private String remark;
    private String source;
    @JsonProperty("SID")
    public String getSID() {
        return SID;
    }
    @JsonProperty("SID")
    public void setSID(String SID) {
        this.SID = SID;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ArrayList<NamesObject> getNames() {
        return names;
    }

    public void setNames(ArrayList<NamesObject> names) {
        this.names = names;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    @Override
    public String toString() {
        return "ObjectItemModel{" +
                "SID='" + SID + '\'' +
                ", _id='" + _id + '\'' +
                ", active='" + active + '\'' +
                ", code='" + code + '\'' +
                ", names=" + names +
                ", objectType='" + objectType + '\'' +
                ", remark='" + remark + '\'' +
                ", source='" + source + '\'' +
                '}';
    }
}
