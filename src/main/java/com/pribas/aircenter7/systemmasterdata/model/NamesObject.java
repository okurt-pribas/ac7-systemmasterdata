package com.pribas.aircenter7.systemmasterdata.model;

/**
 * Oray KURT
 * 28-May-2018
 * com.pribas.aircenter7.systemmasterdata.model
 */
public class NamesObject {
    private String lang;
    private String name;

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "NamesObject{" +
                "lang='" + lang + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
