package com.pribas.aircenter7.systemmasterdata.model;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

/**
 * Oray KURT
 * 23-May-2018
 * com.pribas.aircenter7.systemmasterdata.model
 */
public class NewObjectItem {
    private String objectType;
    private String active;
    private String source;
    private String code;
    private List<Languages> names;
    private String remark;
    private String SID;
    public class Languages{
        private String name;
        private String lang;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getLang() {
            return lang;
        }

        public void setLang(String lang) {
            this.lang = lang;
        }
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<Languages> getNames() {
        return names;
    }

    public void setNames(List<Languages> names) {
        this.names = names;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    @JsonProperty("SID")
    public String getSID() {
        return SID;
    }
    @JsonProperty("SID")
    public void setSID(String SID) {
        this.SID = SID;
    }
}
