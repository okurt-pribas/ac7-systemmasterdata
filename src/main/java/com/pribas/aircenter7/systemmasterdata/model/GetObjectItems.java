package com.pribas.aircenter7.systemmasterdata.model;

/**
 * Oray KURT
 * 22-May-2018
 * com.pribas.aircenter7.systemmasterdata.model
 */
public class GetObjectItems {
    private ParentNode parent;
    private SelfNode self;

    public class ParentNode {
        private String type;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }
    public class SelfNode {
        private String type;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }
    public ParentNode getParent() {
        return parent;
    }

    public void setParent(ParentNode parent) {
        this.parent = parent;
    }

    public SelfNode getSelf() {
        return self;
    }

    public void setSelf(SelfNode self) {
        this.self = self;
    }
}
