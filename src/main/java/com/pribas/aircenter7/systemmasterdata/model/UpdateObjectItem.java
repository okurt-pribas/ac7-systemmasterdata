package com.pribas.aircenter7.systemmasterdata.model;

import java.util.List;

/**
 * Oray KURT
 * 23-May-2018
 * com.pribas.aircenter7.systemmasterdata.model
 */
public class UpdateObjectItem {

    private String SID;
    private String _id;
    private Boolean active;
    private String code;
    private List<Names> name;
    private String objectType;
    private String remark;
    private String source;

    public class Names{
        private String lang;
        private String name;

        public String getLang() {
            return lang;
        }

        public void setLang(String lang) {
            this.lang = lang;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public String getSID() {
        return SID;
    }

    public void setSID(String SID) {
        this.SID = SID;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<Names> getName() {
        return name;
    }

    public void setName(List<Names> name) {
        this.name = name;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    @Override
    public String toString() {
        return "UpdateObjectItem{" +
                "SID='" + SID + '\'' +
                ", _id='" + _id + '\'' +
                ", active=" + active +
                ", code='" + code + '\'' +
                ", name=" + name +
                ", objectType='" + objectType + '\'' +
                ", remark='" + remark + '\'' +
                ", source='" + source + '\'' +
                '}';
    }
}
