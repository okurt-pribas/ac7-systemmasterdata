package com.pribas.aircenter7.systemmasterdata.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.pribas.aircenter7.systemmasterdata.handler.SystemMasterDataCapability;
import com.pribas.aircenter7.systemmasterdata.model.CsvResponse;
import com.pribas.aircenter7.systemmasterdata.model.GetObjectItems;
import com.pribas.aircenter7.systemmasterdata.model.NewObjectItem;
import com.pribas.aircenter7.systemmasterdata.model.ObjectItemModel;
import com.pribas.aircenter7.web.core.model.ErrorCodes;
import com.pribas.aircenter7.web.core.service.model.ServiceResponseCodeEnum;
import com.pribas.aircenter7.web.core.util.JsonResponseProcessor;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Oray KURT
 * 21-May-2018
 * com.pribas.aircenter7.systemmasterdata.controller
 */


@Controller
@RequestMapping(value = "/systemmasterdata/")
public class SystemMasterDataTreeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SystemMasterDataTreeController.class);

    @Autowired
    SystemMasterDataCapability systemMasterDataCapability;


    private JsonParser jsonParser = new JsonParser();
    private JsonObject responseJson = new JsonObject();
    private String responseString;
    private String responseStatusStr;

    @RequestMapping(value = "/getObjectTreeItems", method = RequestMethod.GET)
    public void getObjectTreeItems(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        httpServletResponse.setContentType("application/json;UTF-8");

        responseString = systemMasterDataCapability.getObjectTypes();
        responseJson = (JsonObject) jsonParser.parse(responseString);
        LOGGER.info(responseString);
        responseStatusStr = responseJson.get("responseCode").getAsString();

        if (responseStatusStr.equals(ServiceResponseCodeEnum.getSuccessCode())) {
            JsonResponseProcessor.sendSuccessResponse(httpServletResponse, responseJson, httpServletRequest.getRequestURL().toString());
        } else {
            JsonResponseProcessor.sendErrorResponse(httpServletResponse, ErrorCodes.GENERAL_ERROR, responseJson.get("responseMessage").getAsString(), httpServletRequest.getRequestURL().toString());
        }
    }

    @RequestMapping(value = "/getObjectItems", method = RequestMethod.POST)
    public void getObjectItems(@RequestParam(value = "masterDataParam") String masterDataParam, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        httpServletResponse.setContentType("application/json;UTF-8");



        LOGGER.info(masterDataParam);

        responseString = systemMasterDataCapability.getObjectItems(masterDataParam);
        responseJson = (JsonObject) jsonParser.parse(responseString);
        LOGGER.info(responseString);
        responseStatusStr = responseJson.get("responseCode").getAsString();

        if (responseStatusStr.equals(ServiceResponseCodeEnum.getSuccessCode())) {
            JsonResponseProcessor.sendSuccessResponse(httpServletResponse, responseJson, httpServletRequest.getRequestURL().toString());
        } else {
            JsonResponseProcessor.sendErrorResponse(httpServletResponse, ErrorCodes.GENERAL_ERROR, responseJson.get("responseMessage").getAsString(), httpServletRequest.getRequestURL().toString());
        }
    }

    @RequestMapping(value = "/updateObject", method = RequestMethod.POST)
    public void updateObject(@RequestParam(value = "masterDataParam") String masterDataParam, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        httpServletResponse.setContentType("application/json;UTF-8");

        LOGGER.info(masterDataParam);
        responseString = systemMasterDataCapability.updateObjectItem(masterDataParam);
        responseJson = (JsonObject) jsonParser.parse(responseString);
        LOGGER.info(responseString);

        responseStatusStr = responseJson.get("responseCode").getAsString();

        if (responseStatusStr.equals(ServiceResponseCodeEnum.getSuccessCode())) {
            JsonResponseProcessor.sendSuccessResponse(httpServletResponse, responseJson, httpServletRequest.getRequestURL().toString());
        } else {
            JsonResponseProcessor.sendErrorResponse(httpServletResponse, ErrorCodes.GENERAL_ERROR, responseJson.get("responseMessage").getAsString(), httpServletRequest.getRequestURL().toString());
        }
    }

    @RequestMapping(value = "/saveObjects", method = RequestMethod.POST)
    public void saveObjects(@RequestParam(value = "masterDataParam") String masterDataParam, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        httpServletResponse.setContentType("application/json;UTF-8");

        LOGGER.info(masterDataParam);
        responseString = systemMasterDataCapability.saveObjects(masterDataParam);
        responseJson = (JsonObject) jsonParser.parse(responseString);
        LOGGER.info(responseString);

        responseStatusStr = responseJson.get("responseCode").getAsString();

        if (responseStatusStr.equals(ServiceResponseCodeEnum.getSuccessCode())) {
            JsonResponseProcessor.sendSuccessResponse(httpServletResponse, responseJson, httpServletRequest.getRequestURL().toString());
        } else {
            JsonResponseProcessor.sendErrorResponse(httpServletResponse, ErrorCodes.GENERAL_ERROR, responseJson.get("responseMessage").getAsString(), httpServletRequest.getRequestURL().toString());
        }    }

    @RequestMapping(value = "/saveObject", method = RequestMethod.POST)
    public void saveObject(@RequestParam(value = "masterDataParam") String masterDataParam, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        httpServletResponse.setContentType("application/json;UTF-8");

        NewObjectItem newObjectItem = new Gson().fromJson(masterDataParam, NewObjectItem.class);
        ObjectMapper mapper = new ObjectMapper();
        String newObjectItemStr = mapper.writeValueAsString(newObjectItem);

        LOGGER.info(newObjectItemStr);
        responseString = systemMasterDataCapability.saveObject(newObjectItemStr);
        responseJson = (JsonObject) jsonParser.parse(responseString);
        LOGGER.info(responseString);
        responseStatusStr = responseJson.get("responseCode").getAsString();

        if(responseStatusStr.equals(ServiceResponseCodeEnum.getSuccessCode())){
            JsonResponseProcessor.sendSuccessResponse(httpServletResponse, responseJson, httpServletRequest.getRequestURL().toString());
        }else{
            JsonResponseProcessor.sendErrorResponse(httpServletResponse, ErrorCodes.GENERAL_ERROR, responseJson.get("responseMessage").getAsString(),httpServletRequest.getRequestURL().toString());
        }
        //JsonResponseProcessor.sendSuccessResponse(httpServletResponse, responseJson, httpServletRequest.getRequestURL().toString());

    }

    @RequestMapping(value = "/deleteObjectItem", method = RequestMethod.POST)
    public void deleteObjectItem(@RequestParam(value = "masterDataParam") String masterDataParam, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        httpServletResponse.setContentType("application/json;UTF-8");
        //http://10.3.10.56:8090/masterdata
        LOGGER.info("Object ID =" + masterDataParam);
        responseString = systemMasterDataCapability.deleteObject(masterDataParam);
        responseJson = (JsonObject) jsonParser.parse(responseString);
        LOGGER.info(responseString);

        responseStatusStr = responseJson.get("responseCode").getAsString();

        if (responseStatusStr.equals(ServiceResponseCodeEnum.getSuccessCode())) {
            JsonResponseProcessor.sendSuccessResponse(httpServletResponse, responseJson, httpServletRequest.getRequestURL().toString());
        } else {
            JsonResponseProcessor.sendErrorResponse(httpServletResponse, ErrorCodes.GENERAL_ERROR, responseJson.get("responseMessage").getAsString(), httpServletRequest.getRequestURL().toString());
        }
    }

    @RequestMapping(value = "/getAvailableLanguages", method = RequestMethod.GET)
    public void getAvailableLanguages(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        httpServletResponse.setContentType("application/json;UTF-8");

        responseString = systemMasterDataCapability.getAvailableLanguages();
        responseJson = (JsonObject) jsonParser.parse(responseString);
        LOGGER.info(responseString);

        responseStatusStr = responseJson.get("responseCode").getAsString();

        if (responseStatusStr.equals(ServiceResponseCodeEnum.getSuccessCode())) {
            JsonResponseProcessor.sendSuccessResponse(httpServletResponse, responseJson, httpServletRequest.getRequestURL().toString());
        } else {
            JsonResponseProcessor.sendErrorResponse(httpServletResponse, ErrorCodes.GENERAL_ERROR, responseJson.get("responseMessage").getAsString(), httpServletRequest.getRequestURL().toString());
        }
    }

    @RequestMapping(value = "/getCsvFile/{objectType}", method = RequestMethod.GET)
    public void getCsvFile(@PathVariable(value = "objectType") String objectType, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        httpServletResponse.setContentType("application/json;UTF-8");
        /*
        objectType_All
        "objectType=" + oType
        "objectType=" + oType + "&&SID=" + sID

        objectType_All
         objectType=MealType
         objectType=MealType&&SID=INFX
         */
        LOGGER.info(objectType);
        responseString = systemMasterDataCapability.getObjectItems(objectType);
        responseJson = (JsonObject) jsonParser.parse(responseString);
        systemMasterDataCapability.getCsvFile(responseJson.get("result").toString(), httpServletResponse, objectType);

    }

    @RequestMapping(value = "/getCsvFile", method = RequestMethod.GET)
    public void getCsvFile(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        httpServletResponse.setContentType("application/json;UTF-8");
        responseString = systemMasterDataCapability.getObjectItemsWithoutFilter();
        responseJson = (JsonObject) jsonParser.parse(responseString);
        systemMasterDataCapability.getCsvFile(responseJson.get("result").toString(), httpServletResponse, "All");

    }

    @RequestMapping(value = "/testUploadCsvFile", method = RequestMethod.POST)
    public void testUploadCsvFile(@RequestParam(value= "masterDataParam") String masterDataParam, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        httpServletResponse.setContentType("application/json;UTF-8");
        LOGGER.info(masterDataParam);
        responseString = systemMasterDataCapability.testObjectItemsToImport(masterDataParam);
        responseJson = (JsonObject) jsonParser.parse(responseString);

        responseStatusStr = responseJson.get("responseCode").getAsString();

        if (responseStatusStr.equals(ServiceResponseCodeEnum.getSuccessCode())) {
            JsonResponseProcessor.sendSuccessResponse(httpServletResponse, responseJson, httpServletRequest.getRequestURL().toString());
        } else {
            JsonResponseProcessor.sendErrorResponse(httpServletResponse, ErrorCodes.GENERAL_ERROR, responseJson.get("responseMessage").getAsString(), httpServletRequest.getRequestURL().toString());
        }
    }

    @RequestMapping(value = "/uploadCsvFile", method = RequestMethod.POST)
    public void uploadCsvFile(@RequestParam(value= "masterDataParam") String masterDataParam, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        httpServletResponse.setContentType("application/json;UTF-8");
        LOGGER.info(masterDataParam);
        responseString = systemMasterDataCapability.objectItemsToImport(masterDataParam);
        responseJson = (JsonObject) jsonParser.parse(responseString);

        responseStatusStr = responseJson.get("responseCode").getAsString();

        if (responseStatusStr.equals(ServiceResponseCodeEnum.getSuccessCode())) {
            JsonResponseProcessor.sendSuccessResponse(httpServletResponse, responseJson, httpServletRequest.getRequestURL().toString());
        } else {
            JsonResponseProcessor.sendErrorResponse(httpServletResponse, ErrorCodes.GENERAL_ERROR, responseJson.get("responseMessage").getAsString(), httpServletRequest.getRequestURL().toString());
        }
    }

    @RequestMapping(value="/activate", method = RequestMethod.POST)
    public void activateItems(@RequestParam(value ="masterDataParam") String masterDataParam, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception{
        httpServletResponse.setContentType("application/json;UTF-8");
        LOGGER.info(masterDataParam);
        responseString = systemMasterDataCapability.activateItems(masterDataParam);
        responseJson = (JsonObject) jsonParser.parse(responseString);

        responseStatusStr = responseJson.get("responseCode").getAsString();

        if (responseStatusStr.equals(ServiceResponseCodeEnum.getSuccessCode())) {
            JsonResponseProcessor.sendSuccessResponse(httpServletResponse, responseJson, httpServletRequest.getRequestURL().toString());
        } else {
            JsonResponseProcessor.sendErrorResponse(httpServletResponse, ErrorCodes.GENERAL_ERROR, responseJson.get("responseMessage").getAsString(), httpServletRequest.getRequestURL().toString());
        }
    }

    @RequestMapping(value="/deactivate", method = RequestMethod.POST)
    public void deactivateItems(@RequestParam(value ="masterDataParam") String masterDataParam, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception{
        httpServletResponse.setContentType("application/json;UTF-8");
        LOGGER.info(masterDataParam);
        responseString = systemMasterDataCapability.deactiveItems(masterDataParam);
        responseJson = (JsonObject) jsonParser.parse(responseString);

        responseStatusStr = responseJson.get("responseCode").getAsString();

        if (responseStatusStr.equals(ServiceResponseCodeEnum.getSuccessCode())) {
            JsonResponseProcessor.sendSuccessResponse(httpServletResponse, responseJson, httpServletRequest.getRequestURL().toString());
        } else {
            JsonResponseProcessor.sendErrorResponse(httpServletResponse, ErrorCodes.GENERAL_ERROR, responseJson.get("responseMessage").getAsString(), httpServletRequest.getRequestURL().toString());
        }
    }
}
